from gensim.models import word2vec

print('training')

lines = word2vec.LineSentence('out/sentences')

model = word2vec.Word2Vec(
    lines,
    size=250,
    iter=10,
    min_count=2000
)

model.save('out/model')
