import pandas as pd
import pprint
from gensim.models import word2vec
import codecs
import shutil
import os

print('filter queries:')

with codecs.open('out/football_words', 'r', 'utf-8') as f:
    football_words = f.read().splitlines()

pprint.pprint(football_words)

def isFootballQuery(query_string):
    if isinstance(query_string, str):
        count = 0
        for word in football_words:
            if word in query_string:
                count = count + 1
                if count > 1:
                    return True
    return False


reader = pd.read_csv('football', sep='\t', index_col='datetime',
                     parse_dates=True, chunksize=2**10)

shutil.rmtree('out/football_queries', ignore_errors=True)
os.mkdir('out/football_queries')

i = 0
k = 0
with codecs.open("out/football_queries/all", 'w', 'utf-8') as f:
    for chunk in reader:
        i = i + 1

        # if i > 5:
        #     break

        df = pd.DataFrame(chunk)
        df = df[df['normal_query'].apply(isFootballQuery)]
        
        # f = codecs.open("out/football_queries/part_%s" % divmod(k, 10**5)[0], 'a', 'utf-8')
        
        df.to_csv(f, header=i == 1, sep='\t')
        k = k + df.shape[0]
        print(k)
