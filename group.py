import pandas as pd
import pprint
from dateutil import parser
import codecs
import os
import shutil

def hasDate(query_string):
  class RuParserInfo(parser.parserinfo):
    MONTHS = [("июня", "июнь"), ("июля", "июль")]
    def validate(self, res):
      if res.month and res.day:
        return True
      return False

  try:
    parser.parse(str(query_string), fuzzy=True, parserinfo=RuParserInfo())
  except:
    return False

  return True

def hasSchedule(query_string):
  return any(x in query_string for x in ['расписание'])

def hasResults(query_string):
  return any(x in query_string for x in ['таблица', 'результат', 'счет'])

def hasVideo(query_string):
  return any(x in query_string for x in ['трансляция', 'онлайн', 'смотреть', 'повтор'])

def hasTeamMembers(query_string):
  return any(x in query_string for x in ['состав', 'сборная', 'команда'])

def hasTeam(query_string):
  teams = ['Россия','Саудовская Аравия','Египет','Уругвай','Португалия','Испания','Марокко','Иран','Франция','Австралия','Перу','Дания','Аргентина','Исландия','Хорватия','Нигерия','Бразилия','Швейцария','Коста-Рика','Сербия','Германия','Мексика','Швеция','Республика Корея','Бельгия','Панама','Тунис','Англия','Польша','Сенегал','Колумбия','Япония']
  for team in teams:
    if team.lower() in query_string:
        return True
  return False

def hasTwoTeams(query_string):
  teams = ['Россия','Саудовская Аравия','Египет','Уругвай','Португалия','Испания','Марокко','Иран','Франция','Австралия','Перу','Дания','Аргентина','Исландия','Хорватия','Нигерия','Бразилия','Швейцария','Коста-Рика','Сербия','Германия','Мексика','Швеция','Республика Корея','Бельгия','Панама','Тунис','Англия','Польша','Сенегал','Колумбия','Япония']
  count = 0
  for team in teams:
    if team.lower() in query_string:
      count = count + 1
      if count > 1:
        return True
  return False

def hasCityOrStadium(query_string):
  cities = ['Москва', 'Калининград', 'Санкт-Петербург', 'Нижний Новгород', 'Волгоград', 'Казань', 'Самара', 'Саранск', 'Ростов-на-Дону', 'Сочи', 'Екатеринбург']
  stadiums = ['стадион', 'арена', 'лужники', 'фишт']
  for cityOrStadium in cities + stadiums:
    if cityOrStadium.lower() in query_string:
        return True
  return False

groups = {
  'schedule': pd.DataFrame([]),
  'results': pd.DataFrame([]),
  'teams': pd.DataFrame([]),
  'match': pd.DataFrame([]),
  'other': pd.DataFrame([]),
}

reader = pd.read_csv('out/football_queries/all', sep='\t', chunksize=2**10)

i = 0
lines = 0
for chunk in reader:
  i = i + 1

  if i > 2:
      break

  football_queries = pd.DataFrame(chunk)

  for index, row in football_queries.iterrows():
    lines = lines + 1
    print(f"{i} / {lines}")
    
    if hasDate(row['normal_query']) or hasVideo(row['normal_query']) or hasCityOrStadium(row['normal_query']) or hasSchedule(row['normal_query']):
      groups['schedule'] = groups['schedule'].append(row)
    
    elif hasResults(row['normal_query']):
      groups['results'] = groups['results'].append(row)
    
    elif hasTwoTeams(row['normal_query']):
        groups['match'] = groups['match'].append(row)

    elif hasTeam(row['normal_query']) or hasTeamMembers(row['normal_query']):
      groups['teams'] = groups['teams'].append(row)
    
    else:
      groups['other'] = groups['other'].append(row)

shutil.rmtree('out/football_queries/groups', ignore_errors=True)
os.mkdir('out/football_queries/groups')

for c, df in groups.items():
  print(f"{c}: {df.shape[0]} lines")
  f = codecs.open(f'out/football_queries/groups/{c}', 'a', 'utf-8')
  df.to_csv(f, index=False, sep='\t')

classified = groups['schedule'].shape[0] + groups['results'].shape[0] + groups['teams'].shape[0]
not_classified = groups['other'].shape[0]  

print(f"rate: {classified / (classified + not_classified) * 100}%")