import pandas as pd
import matplotlib.pyplot as plt
import tkinter
import pprint
import codecs

dfs = {
  'schedule': pd.DataFrame(pd.read_csv('out/football_queries/groups/schedule', sep='\t', index_col='datetime', parse_dates=True)),
  'results': pd.read_csv('out/football_queries/groups/results', sep='\t', index_col='datetime', parse_dates=True),
  'teams': pd.read_csv('out/football_queries/groups/teams', sep='\t', index_col='datetime', parse_dates=True),
  'match': pd.read_csv('out/football_queries/groups/match', sep='\t', index_col='datetime', parse_dates=True),
  'other': pd.read_csv('out/football_queries/groups/other', sep='\t', index_col='datetime', parse_dates=True)
}


for c, df in dfs.items():
  print(f"{c}: {df.info()} lines")
  df = df.resample('d')['normal_query'].size()
  df.plot(label=c)

plt.legend()
plt.show()