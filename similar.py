from gensim.models import word2vec
import codecs

print('similar:')

model = word2vec.Word2Vec.load('out/model')

popular = model.wv.most_similar(
    ['футбол', 'чемпионат', 'чм'], topn=100)

with codecs.open('out/football_words', 'w', 'utf-8') as f:
    for word in popular:
        f.write("%s\n" % word[0])
