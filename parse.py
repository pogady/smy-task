import pandas as pd
import pprint
from gensim.models import word2vec
import codecs
import os


print('parse queries:')

reader = pd.read_csv('football', sep='\t', index_col='datetime',
                     parse_dates=True, chunksize=2**10)

os.mkdir('out')

i = 0
with codecs.open('out/sentences', 'w', 'utf-8') as f:
    for chunk in reader:
        i = i + 1
        # if i > 50:
        #     break
        print(i)
        df = pd.DataFrame(chunk)
        for item in df['normal_query'].values:
            f.write("%s\n" % item)
